# Installation Guide
## Windows
1. Install postgres using the postgres installer and create a password
2. Run the sql shell
3. on `Server [localhost]` press enter
4. on `Database [postgres]` press enter
5. on `Port [port]` press enter
6. on `Username [postgres]` press enter
7. on `Password: ` enter password
8. Run command `postgres#: CREATE USER user_name;`
    * note: the semicolon `;` is important
9. 