module github.com/emendoza/classmanager

go 1.12

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/graphql-go/graphql v0.7.7
	github.com/graphql-go/handler v0.2.3 // indirect
	github.com/lib/pq v1.0.0
	github.com/mattn/go-sqlite3 v1.10.0
	golang.org/x/crypto v0.0.0-20190320223903-b7391e95e576
	gopkg.in/yaml.v2 v2.2.2
	gopkg.in/yaml.v3 v3.0.0-20190502103701-55513cacd4ae
)
